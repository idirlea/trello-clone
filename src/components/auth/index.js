import Auth from './Auth.vue';
import Login from './Login.vue';
import Registration from './Registration.vue';

export {
  Auth,
  Login,
  Registration
};

export default Auth;