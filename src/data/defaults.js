import uuidv4 from 'uuid/v4';

const defaultBoard = {
  name: 'Welcome to trello',
  id: uuidv4(),
  userId: null,
  lists: [
    {
      id: uuidv4(),
      name: 'Starter list',
      items: [
        { id: uuidv4(), name: 'This is your first item', status: 0, order: 1 },
        { id: uuidv4(), name: 'This is your second item', status: 0, order: 2 }
      ]
    },
    {
      id: uuidv4(),
      name: 'Another one',
      items: [
        { id: uuidv4(), name: 'Should you add another one?', status: 0, order: 1 },
      ]
    }
  ]
};

export const generateDefaultBoard = (userId) => ({...defaultBoard, userId });