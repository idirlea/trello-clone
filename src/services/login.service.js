import uuidv4 from 'uuid/v4';
import lscache from 'lscache';

import { generateDefaultBoard } from './../data/defaults';

export const regiterUser = (user) => {
  const users = lscache.get('users') || [];
  const userId = uuidv4();
  const token = uuidv4();
  const hasUser = users.find(u => u.email === user.email);

  if (!hasUser) {
    // store user in storage
    lscache.set('users', [
      ...users ,
      { ...user, id: userId, token, creationDate: Date.now(), updatedDate: Date.now() }
    ]);

    // store token for 60 min
    lscache.set('token', token, 60);

    // store new board
    const board = generateDefaultBoard(userId);
    lscache.set('boards', [...(lscache.get('boards') || []), board ]);

    return true;
  }

  return false;
}

export const loginUser = (email, pass) => {
  const users = lscache.get('users') || [];
  const currentUser = users.find((user) => user.email === email && user.password === pass);

  if (currentUser) {
    const token = uuidv4();

    // store user in storage
    lscache.set('users', [
      ...users.filter((user) => user.id !== currentUser.id ),
      { ...currentUser, token, updatedDate: Date.now() }
    ]);

    // store token for 60 min
    lscache.set('token', token, 60);

    return true;
  }

  return false;
}