import lscache from 'lscache';

export const getBoards = () => {
  const token = lscache.get('token');
  const user = lscache.get('users').find(u => u.token === token);

  return lscache.get('boards').filter(board => board.userId === user.id);
}