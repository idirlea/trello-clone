import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import store from './store';

import Board from './components/Board.vue';
import Profile from './components/Profile.vue';

export const Bus = new Vue();

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  { path: '/', component: Board },
  { path: '/profile', component: Profile },
]

const router = new VueRouter({
  routes // short for `routes: routes`
})
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
